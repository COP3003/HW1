import java.awt.Point;
import java.awt.event.ActionListener;
import java.util.function.BiPredicate;

/**
 * Tile Pieces interface for tracking and interacting with tiles
 */
interface Tiles {
  BiPredicate<Point, Point> moveTest = (selected, empty) ->
    (selected.x == empty.x && Math.abs(selected.y - empty.y) == 0) ||
    (selected.y == empty.y && Math.abs(selected.x - empty.x) == 0);
  default String getType() {
    return getClass().getSimpleName();
  }
  boolean move(EmptyTile e);
  int getId();
  void addListener(ActionListener al);
  void updateLocation(int position, Point p);
}
