import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.util.function.BiPredicate;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Class that generates a tile that has an image and is interactable on the
 *  game board.
 */
class ButtonTile extends JButton implements Tiles{
  ImageIcon icon; // retains a reference to the original icon object
  final int id; // position with respect to the original image
  int currentPosition; // current position on the game board
  int x; // Syntactic sugar
  int y; // Syntactic sugar

  /**
   * Creates a Tile that is a JButton object and attaches an icon of a portion
   *  of the image to it.
   *
   * @param id  grid location from the original image
   * @param cp  initial grid position
   * @param p   topleft point of the image to generate the icon from
   * @param width   icon width
   * @param height  icon height
   * @param image   original image to extract icon from
   */
  ButtonTile(int id, int cp, Point p, int width, int height, 
      BufferedImage image){
    this.id = id; // position with respect to the original image
    currentPosition = cp; // current position on the game board
    this.x = p.x;
    this.y = p.y;

    // generate the icon from a portion of the image
    icon = new ImageIcon();
    icon.setImage(image.getSubimage(p.x, p.y, width, height));

    setIcon(icon);  // attach icon to the button
    setPreferredSize(new Dimension(width, height)); // might be needed for pack
  }

  /**
   * Checks if the empty tile is adjacent with this tile. If so it changes the
   *  point position and grid position variables with the empty tile.
   *
   * @param e   emptyTile  empty tile to swap with
   * @return    boolean    true if the tiles were adjacent
   */
  public boolean move(EmptyTile e) {
    boolean hasMoved = false;
    if (moveTest.test(getLocation(), e.getLocation())) {
      int temp = e.currentPosition;
      Point pos = e.getLocation();
      e.updateLocation(currentPosition, pos.getLocation());
      updateLocation(temp, pos.getLocation());
      hasMoved = true;
    }
    return hasMoved;
  }

  /**
   * Tests if the two points are adjacent, horizontal or vertical.
   */
  private BiPredicate<Point, Point> moveTest = (selected, empty) ->
    (selected.x == empty.x && Math.abs(selected.y - empty.y) == getHeight()) ||
    (selected.y == empty.y && Math.abs(selected.x - empty.x) == getWidth());
  
  public int getId() {
    return id;
  }
  
  public void addListener(ActionListener al) {
    addActionListener(al);
   }

  /**
   * Sets the tile's point position and grid position variables to a different
   *  grid.
   * @param position grid position
   * @param p topleft point of new grid position
   */
  public void updateLocation(int position, Point p) {
    setLocation(p.getLocation());
    this.x = p.x;
    this.y = p.y;
    currentPosition = position;
  }
}