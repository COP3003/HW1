

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.Toolkit;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Point;


public class IconButtonFrame extends JFrame{
	private JButton button;
  List<ImageIcon> iconList = new ArrayList<>();
  List<JButton> btns = new ArrayList<>();

	// where to start extracting the icon in the image
	private static final int leftTopX = 20;
	private static final int leftTopY = 20;

	// the size of the icon
	private static final int width = 100;
	private static final int height = 100;
	
	


	private  Icon extractIcon(String path){
		// reads the image into a BufferedImage object
		BufferedImage image=null;
		try{
			image = ImageIO.read(new File(path));
		}catch(IOException e){
			System.err.println("Image not found");
			System.exit(1);
		}

		// allocates another BufferedImage object whose size is
		// the same as the one of the wanted icon
		BufferedImage part = new BufferedImage(
				width, height, BufferedImage.TYPE_4BYTE_ABGR
		);
		
		// copies the data from "image" to "part"
		for(int x=0;x<width;x++){
			for(int y=0;y<height;y++){
				part.setRGB(x,y, image.getRGB(x+leftTopX, y+leftTopY));
			}
		}

		// creates an icon whose content is already in "part"
		ImageIcon icon = new ImageIcon();
		icon.setImage(part);
		icon.setImage(image.getSubimage(0, 0, 150, 100));

		// returns to the caller
		return icon;
		
	}

	/**
	 * This constuctor requires the path to an existing image file.
	 *
	 * @param imagePath  path to the image file
	 */
	public IconButtonFrame(String imagePath){
		super( "Icon from image" ); // set the title

		Icon icon = extractIcon(imagePath); // finds the icon
	  initComponents(icon);
	}
	private void initComponents(Icon icon) {
	  setIconImage(Toolkit.getDefaultToolkit().getImage("whatyousay.jpg"));
	  getContentPane().setLayout(new GridLayout(5, 5, 0, 0));
	  JToggleButton tglbtnMeow = new JToggleButton();
	  
	  
	  tglbtnMeow.setSelectedIcon(new ImageIcon("FGCU_logo.png"));
	  tglbtnMeow.setIcon(new ImageIcon("whatyousay.jpg"));
	  tglbtnMeow.setVerticalAlignment(SwingConstants.BOTTOM);
	  getContentPane().add(tglbtnMeow);
	  button = new JButton(icon); // creates a button with the icon
	  getContentPane().add(button); // adds the button to the frame
	  button.setLocation(100,100);
	  
	  
	  

    BufferedImage immage = null;
    try {
      immage = ImageIO.read(new File("whatyousay.jpg"));
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

	  for (int y = 0; y < 3; y++) {
	    for (int x = 0; x <3; x++) {
	      ImageIcon ic = new ImageIcon();
	      ic.setImage(immage.getSubimage(x*width, y*height, width, height));
//        icon.setImage(image.getSubimage(x, y, width, height));//[x,y]);
        iconList.add(ic);
	    }
	  }
	  for (int i = 0;i<iconList.size();i++) {
	    System.err.println(iconList);
	  }
//	  iconList.stream().mapToInt(n -> n.indexOf()).forEach(System.err::println);
//      System.err.println(iconList);
      iShuffle();
//      System.err.println(iconList);

      iconList.forEach(System.err::println);
      int c = 0;
      for (ImageIcon i : iconList) {
        btns.add(new JButton(i));
        getContentPane().add(btns.get(i), c);
      }
      btns.forEach(btn -> getContentPane().add(btn));
      
//    button.setIcon(new ImageIcon("whatyousay.jpg"));
	}

public void iShuffle() {
      Collections.shuffle(iconList);
      Collections.shuffle(iconList);
}

	public static void main( String args[] ){
	   IconButtonFrame buttonFrame = new IconButtonFrame("fgcu_logo.png");
	   buttonFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
	   buttonFrame.setSize( 500, 500 ); // set frame size
	   buttonFrame.setVisible( true ); // display frame
//	   Point p1 = new Point();
//	   Point p2 = new Point(p1);
//	   p1.setLocation(100, 100);
//	   p2.setLocation(400,400);
//	   System.err.println(p1 + " <-p1, p2-> " + p2);
//	   String st = "blah";
//	   st = st.replace("l", "h");
//	   System.err.println(st);
	}
}
