import java.awt.Component;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

/**
 * todo add comments to JEightPuzzleFrame
 * todo remove unused code
 */
public class JEightPuzzleFrame /* extends JFrame implements ActionListener */ {
  private final GameBoard board;

  // Dimensions that each tile occupies
//  private final int tileWidth;
//  private final int tileHeight;

  private List<Tiles> tiles; // ordered(by id) list of tile objects
//  private Map<Integer, Tiles> currentPositions; // <gridPosition.index, tiles>
  private List<Tiles> currentPositions; // <tiles>

  // constructor
  public JEightPuzzleFrame(String title, String path) {
    BufferedImage image = null; // image to be loaded from path argument

    // reads the image into a BufferedImage object or bust
    try {
      image = ImageIO.read(new File(path));
    } catch (IOException e) {
      System.err.println("Image not found");
      System.exit(1);
    }

    int[] initialPositions = parseIntArray("912563478");
    String emptyTileImagePositions = "9";

    board = new GameBoard(3, 3, title); // create a new 3x3 puzzle board
    board.setSize(image.getWidth(), image.getHeight());

    // set tile dimensions
    int tileWidth = board.getWidth() / board.cols;
    int tileHeight = board.getHeight() / board.rows;

    // If the image doesn't divide into evenly sized pieces, truncation
    // will take place. These values help distribute the lost pixels
    // between the respective sides of the image.
    Point offSet = new Point((board.getWidth() == board.cols * tileWidth)?
                         0: (board.getWidth() - board.cols * tileWidth) / 2,
                         (board.getHeight() == board.rows * tileHeight)?
                         0: (board.getHeight() - board.rows * tileHeight) / 2);

    tiles = new ArrayList<>();
    // initialize the gridPositions array
    board.initializePositions(offSet.getLocation(), tileWidth, tileHeight);

    // create new Tile objects for each tile piece and add to tiles array
    // this also generates the button icons from the image.
    for (int i = 0; i < initialPositions.length; i++) {
      int tileId = initialPositions[i];
      if (emptyTileImagePositions.contains(Integer.toString(tileId))) {// empty tile
        tiles.add(new EmptyTile(tileId, i+1, board.getPosition(tileId - 1), tileWidth,
                                tileHeight));
      } else { // button tile
        tiles.add(new ButtonTile(tileId, i+1, board.getPosition(tileId - 1), tileWidth,
                                 tileHeight, image));
      }
    }

    currentPositions = new ArrayList<>();
    for (Tiles t : tiles) {
      currentPositions.add(t);
    }
    tiles.forEach(tile -> board.add((Component) tile));
    board.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    board.pack();
    board.setVisible(true);

    ButtonTileHandler buttonTileHandler = new ButtonTileHandler();
    
    // post-creation tile processing
    for (Tiles tile : tiles) {
      if (tile.getClass().getSimpleName().equals("ButtonTile")) {
        tile.addListener(buttonTileHandler); // register listener
      }
    }
  }

  private class ButtonTileHandler implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent event) {
      // check event source and lay out content pane correspondingly
//      /*****************TESTER******************************/
//      for (int i = 0; i < currentPositions.size(); i++) {
//        System.err.printf("%s \t<-gpid [%d] cp->\t%d\t\n",currentPositions.get(i).getId(), i,
//            (currentPositions.get(i).getType().equals("EmptyTile")? 
//             ((EmptyTile) tiles.get(i)).currentPosition:
//              ((ButtonTile) tiles.get(i)).currentPosition));
//      } System.err.println("------------------------------------");
//      /*****************TESTER******************************/
      for (Tiles tile : tiles) {
        int t = (tile.getType().equals("ButtonTile"))?((ButtonTile) tile).currentPosition-1:0;
        if (event.getSource() == tile) {
          for (Tiles empty : tiles) {
            int e = (empty.getType().equals("EmptyTile"))?((EmptyTile) empty).currentPosition-1:0;
            if (empty.getType().equals("EmptyTile") && tile.move(
                (EmptyTile) empty)) {
                currentPositions.set(e, tile);
                currentPositions.set(t, empty);
//                System.err.printf("t: %d\t Type: %s\n\t%s\n",t,tile.getClass(),tile);
//                System.err.printf("e: %d\t Type: %s\n\t%s\n",e,empty.getClass(),empty);
//                /*****************TESTER******************************/
//                for (int i = 0; i < currentPositions.size(); i++) {
//                  System.err.printf("%s \t<-gpid [%d] cp->\t%d\t\n",currentPositions.get(i).getId(), i,
//                      (currentPositions.get(i).getType().equals("EmptyTile"))?((EmptyTile) currentPositions.get(i)).currentPosition:
//                        ((ButtonTile) currentPositions.get(i)).currentPosition);
//                } System.err.println("------------------------------------");
//                /*****************TESTER******************************/
                
//                /*****************TESTER******************************/
//                for (int i = 0; i < currentPositions.size(); i++) {
//                  System.err.printf("%s \t<-gpid [%d] cp->\tx\t\n",currentPositions.get(i).getId(), i);
//                } System.err.println("------------------------------------");
//                /*****************TESTER******************************/
              break;
            }
          }
        }
      }
//      board.removeAll();
//
      currentPositions.forEach(tile -> board.add((Component) tile));
      board.setLayout(board.getBoardLayout());
//      board.container.getComponent(tiles.get(0));
//      board.getContentPane().removeall();
      board.validate(); // re-lay out container
    }
  }

  /**
   * Converts an integer string to an array of digits.
   *
   * It does not check the validity of the string.
   *
   * @param number
   *          integer string
   */
  private static int[] parseIntArray(String number) {
    return java.util.regex.Pattern.compile("").splitAsStream(number).mapToInt(
        Integer::parseInt).toArray();
  }

  public static void main(String[] args) {
    // JEightPuzzleFrame gridLayoutFrame = new JEightPuzzleFrame("blah",
    // "fgcu_logo.png");
    // JEightPuzzleFrame gridLayoutFrame = new
    new JEightPuzzleFrame("blah", "whatyousay.jpg");
  }
}

/*
 //Dialog Box "win".
        win = new Dialog(this, "You Win!", false);
        win.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                win.setVisible(false);
            }
        });
        win.setSize(150, 100);
        win.add(new Label("You Win!"),BorderLayout.CENTER);
  ---------------------------------------------------------------
  public void itemStateChanged(ItemEvent e) {
        repaint();
    }
    
     // Show Winner optionPane
            JOptionPane optionPane = new JOptionPane("Congratulation !",
                    JOptionPane.INFORMATION_MESSAGE);
            JDialog dialog = optionPane.createDialog("Winner");

            // When user click "OK",
            dialog.addComponentListener(new ComponentListener() {
*/