import java.awt.Container;
import java.awt.GridLayout;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

//we're assuming a square image therefore numRows == numCols
// going to pull out anything relating to the initializing the puzzle game board
public class GameBoard extends JFrame {
  // potentially user defined references
  final int rows;
  final int cols;
  final int numberOfPositions; // number of grid locations
  Container container;
  // swing components
  private final GridLayout gridLayout;

  // position related references
  /*
   * Grid squares are in order from top-left to bottom-right. The Point is the
   * top left pixel of the square. 
   * _____________ 
   * | 1 | 2 | 3 |
   * -------------
   * | 4 | 5 | 6 |
   * -------------
   */
  
  private final List<Point> gridPositions; // ordered(by id) <topLeftPoint>

  /**
   * Virtual surface defined by a grid layout to place the game pieces on.
   *
   * @param rows    int
   * @param cols    int
   * @param title   String  title on the JFrame window
   */
  GameBoard(int rows, int cols, String title) {
    super(title);       // JFrame constructor that sets the window title
    this.rows = rows;
    this.cols = cols;
    numberOfPositions = rows * cols;

    Container container = getContentPane();   // the grid is attached to this
    gridLayout = new GridLayout(rows, cols);  // effectively the game surface
    setLayout(gridLayout);                    // defaulting to 3 by 3; no gaps
    gridPositions = new ArrayList<>(numberOfPositions);
  }

  /**
   * Populates the gridPositions ArrayList with Points representing the topleft
   *  corner of each grip position in the default grid order.
   *
   * @param p       topleft point of grid 1, not always 0,0
   * @param width   used to generate x position and test if at the end of a row
   * @param height  used to calculate y position
   */
  void initializePositions(Point p, int width, int height) {
    int xOffset = p.x;  // starting x value for column 1
    for (int i=0; i < numberOfPositions; i++) {
      gridPositions.add(p.getLocation()); // add the current point to the list

      // test if in the last column and if so advance to next row
      if ((gridPositions.get(i).x + width) % (cols * width) == 0) {
        p.x = xOffset;
        p.y += height;
      } else {
        p.x += width;
      }
    }
  }

  /**
   * gridLayout getter
   * @return gridLayout gridLayout reference
   */
  GridLayout getBoardLayout() {
    return gridLayout;
  }

  /**
   * gridPositions getter
   * @return gridPositions gridPositions reference
   */
  List<Point> getPositions(){
    return new ArrayList<>(gridPositions);
  }

  /**
   * gets the coordinates of the point stored at a specific index in
   *  the gridPositions list.
   * @param id int index of the coordinates to retrieve
   * @return Point Point coordinates
   */
  Point getPosition(int id) {
    return gridPositions.get(id).getLocation();
  }

  /**
   * uses a coordinate point object to get the grid location
   * @param p Point Point coordinate attached to an index
   * @return  int grid location
   */
  int getPositionIndex(Point p) {
    return gridPositions.indexOf(p);
  }
}