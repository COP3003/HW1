import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

/**
 * Class that generates a tile that has no image and is not interactable on the
 *  game board.
 */
public class EmptyTile extends JButton implements Tiles {
//  final ImageIcon icon;  // delete if this isn't used

  final int id; // position with respect to this missing (empty) image piece
  int currentPosition; // current grid location on the game board
  int x; // Syntactic sugar
  int y; // Syntactic sugar

  /**
   *
   * @param id  grid location from the original image
   * @param cp  initial grid position
   * @param p   topleft point of the image this tile represents
   * @param width   tile width
   * @param height  tile height
   */
  EmptyTile(int id, int cp, Point p, int width, int height) {
//    icon = null;    // delete with the above class reference
    this.id = id;         // position with respect to the original image
    currentPosition = cp; // current grid location on the game board
    this.x = p.x;
    this.y = p.y;

    setPreferredSize(new Dimension(width, height)); // might be needed for pack
    setVisible(false); // empty tiles are not visible
  }

  /**
   * Blank interface method from Tiles. Not used.
   */
  public boolean move(EmptyTile e) {
    return false;
  }

  public int getId() {
    return this.id;
  }

  /**
   * Blank interface method from Tiles. Empty tiles can't act.
   */
  public void addListener(ActionListener al) {
  }

  /**
   * Sets the tile's point position and grid position variables to a different
   *  grid.
   * @param position grid position
   * @param p topleft point of new grid position
   */
  public void updateLocation(int position, Point p) {
    setLocation(p.getLocation());
    this.x = p.x;
    this.y = p.y;
    currentPosition = position;
  }
}
